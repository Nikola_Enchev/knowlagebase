import './App.css';
import MyButton from './components/button';

function App() {

  const printAccept = () => {
    console.log('Accept');
  }

  const printDecline = () => {
    console.log('Decline');
  }

  return (
    <div className="App">
      <MyButton type={'accept'} children={'Accept'} onClick={printAccept}/>

      <MyButton type={'decline'} children={'Decline'} onClick={printDecline}/>
    </div>
  );
}

export default App;

