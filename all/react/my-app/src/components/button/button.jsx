import React from 'react';

const MyButton = ({ type, onClick, children }) => {
  const buttonStyle = {
    backgroundColor: type === 'accept' ? 'green' : 'red',
    color: 'white',
    padding: '10px',
    borderRadius: '5px',
    cursor: 'pointer',
  };

  return (
    <button style={buttonStyle} onClick={onClick}>
      {children}
    </button>
  );
};

export default MyButton;