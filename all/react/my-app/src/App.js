import './App.css';
import ItemList from './components/list/list';
import MyButton from './components/button/button';

function App() {
  const items = ['item1', 'item2', 'item3'];

  const handleAccept = () => { 
    console.log('Accept');
  };

  return (
    <div className="wrapper">
      <button className="button-accept">Accept</button>

      <button className="button-reject">Reject</button>

      <MyButton type={'green'} children={'Accept'} onClick={handleAccept}></MyButton>
      <ItemList items={items}></ItemList>
    </div>
  );
}

export default App;
