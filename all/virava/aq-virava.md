

initializeApp(authService, appConfigService, apiConfig) )
 -> 
AppConfigService
loadConfig()
 sends request to retrieve config.json file

getKeycloakConfig()
  returns object with baseUrl, redirectUri, realm, clientId

AuthService
 init() ->
 init viravaService with config generated with getKeycloakConfig()
 viravaService.login()


