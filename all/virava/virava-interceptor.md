Old way:
401 -> 
if refresh token has not expired -> update token 
  -> if token updated successfully -> retry the failed request with the new token
  -> if token not valid failed -> logout
  -> if token update failed -> logout
if refresh token expired -> logout

New way:
401 ->

if refresh token not expired -> update token
  -> if token updated successfully -> retry the failed request with the new token
  -> if request fails -> throw error

if refresh token expired -> logout user