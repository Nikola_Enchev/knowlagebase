Missing id token hint issue
  This error message appears inconsistently.

Keycloak needs id_token_hint in order to know which session it needs to end.

  ```typescript
return this.keycloak.logout({ redirectUri: redirectUri }).then(() => {
  removeTokens(this.config);
});
  ```

Keycloak JS(adapter) automatically includes id_token when we use the `logout()` method

  ```js
kc.createLogoutUrl = function(options) {
    var url = kc.endpoints.logout()
        + '?client_id=' + encodeURIComponent(kc.clientId)
        + '&post_logout_redirect_uri=' + encodeURIComponent(adapter.redirectUri(options, false));

    if (kc.idToken) {
        url += '&id_token_hint=' + encodeURIComponent(kc.idToken);
    }

    return url;
};
  ```
Possible solutions:

  Check where id_token is stored.

  Check if id_token has another actual name(like client_id)

  Check if the session is expired. Then id_token will not be available

  Update versions of Keycloak js

  Not sending redirect_uri might fix the issue (this way we should get a message asking us if we want to logout)



  On login we get client_id : Auth_LHT
  From the config we get : keycloakClientId: autoquote
  We sent a token with client_id : autoquote (why don't we send it with Auth_LHT ?)
  We get access_token, id_token, refresh_token


Reference the screenshots in assets folder.