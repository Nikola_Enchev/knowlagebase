# Unit testing in Angular with Jest

describe - allows logically grouping tests in a test suite

mocking 
  - allows us to avoid testing code that is outside of our control
  - get reliable return values
  - used for async code
```typescript

  describe( description: string, specDefinitions: () =>  {
    // fixture can represent a component, directive, pipe or service
    let fixture: ComponentName;

    // mock the component dependencies
    let componentDependencyMock;

    // beforeEach is called before each test
    beforeEach(() => {
      componentDependencyMock = {
        // mocks the function inside a dependency
        someFunction: jest.fn()
      } as ComponentDependency;

      // create a new instance of the component before each test
      fixture = new ComponentName(componentDependencyMock);

    });

    // Component setup tests
    describe('Component setup', () => {

      // test the component is created
      it('ngOnInit', () => {
        it('should call someFunction', () => {
          
          // Arrange
          // watching something for a change
          const someFunctionSpy = jest.spyOn(fixture, 'someFunction');
          const someVar = {
            id: 1,
            name: 'someName'
          } as SomeType;
          fixture.someVar = someVar;
          
          // Act
          fixture.ngOnInit();

          // Assert
          expect(someFunctionSpy).toHaveBeenCalledWith(someVar);
        });
      });
    });

    describe('sendRequest' () => {
      describe('send invalid data', () => {
        it('should throw an error', () => {
          // Arrange
          const someInvalidVar = {
            id: 1,
            name: 'someName'
          } as SomeType;
          fixture.someVar = someInvalidVar;
          
          // Act
          fixture.sendRequest();

          // Assert
          expect(componentDependencyMock.someFunction).not.toHaveBeenCalled();
        }
      })

      describe('send valid data', () => {
        it('should throw an error', () => {
          // Arrange
          const someValidVar = {
            id: 1,
            name: 'someName'
          } as SomeType;
          fixture.someVar = someValidVar;

          // takes something async and makes it act in a sync way
          componentDependencyMock.someFunction.mockReturnValue(of({ args: 'someArgs'}));

          // Act
          fixture.sendRequest();

          // Assert
          expect(componentDependencyMock.someFunction).toHaveBeenCalled();
        }
      })
    });

  });

```