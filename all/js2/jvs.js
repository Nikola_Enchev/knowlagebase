// let const var

// var a = 10;

let a = 10;

for (let i = 0; i < 10; i++) {
    console.log(i);
}

let arr = [1, 2, 3, 4, 5];

let obj = {
  name: 'pesho',
  age: 20
};

const func = (a) => { 
  console.log(a);
}
